﻿using UnityEngine;

namespace Pump.Unity
{
    public static class ColorUtils
    {
        public static Color SetAlpha(this Color color, float a)
        {
            color.a = a;
            return color;
        }
        public static Color MultiplyAlpha(this Color color, float a)
        {
            color.a *= a;
            return color;
        }
    }
}
