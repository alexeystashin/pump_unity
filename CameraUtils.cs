﻿using UnityEngine;

namespace Pump.Unity
{
    public static class CameraUtils
    {
        // target - point 
        // distance - distance from target to camera
        // pitchAngle - vertical angle in degrees
        // yawAngle - horizontal rotation angle in degrees
        // rollAngle - front rotation angle in degrees
        public static void SetupCamera(Camera camera, Vector3 targetPos, float distance, float pitchAngle, float yawAngle, float rollAngle, float fov = 0)
        {
            var rot = Quaternion.Euler(pitchAngle, yawAngle, rollAngle) * Vector3.forward;
            var pos = targetPos - rot * distance;

            camera.transform.position = pos;
            camera.transform.LookAt(targetPos);

            if (fov != 0)
                camera.fieldOfView = fov;
        }

        public static void Shake(Camera camera, float shakePower = 0.5f, float shakeTime = 0.5f, float decreaseFactor = 1.0f, bool useOriginalPos = true)
        {
            var cameraShake = camera.GetComponent<CameraShake>();
            if (cameraShake != null)
            {
                // skip if already have more powerful shake
                if (cameraShake.shakeAmount > shakePower)
                    return;

                GameObject.Destroy(cameraShake);
            }
            cameraShake = camera.gameObject.AddComponent<CameraShake>();
            cameraShake.camTransform = camera.transform;
            cameraShake.shakeAmount = shakePower;
            cameraShake.shakeDuration = shakeTime;
            cameraShake.decreaseFactor = decreaseFactor;
            cameraShake.useOriginalPos = useOriginalPos;
        }
    }
}
