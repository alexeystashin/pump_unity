﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pump.Unity
{
    public class SpriteCache
    {
        #region singleton
        private static SpriteCache _instance;
        public static SpriteCache instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SpriteCache();
                return _instance;
            }
        }
        private SpriteCache() { }
        #endregion

        Dictionary<string, Sprite> sprites = new();
        Dictionary<string, Dictionary<string, Sprite>> spriteSheets = new();

        public Sprite GetSprite(string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;

            if (!path.Contains(":"))
                return GetSpriteFromFile(path);

            var t = path.Split(':');
            if (t.Count() != 2)
                throw new Exception("SpriteCache:GetSprite(" + path + ")");
            //DebugUtils.Log("SpriteCache:GetSprite(" + t[0] + "| " + t[1] + ")");
            return GetSpriteFromAtlas(t[0], t[1]);
        }

        public Sprite GetSpriteFromFile(string path)
        {
            if (sprites.ContainsKey(path))
                return sprites[path];

            var sprite = Resources.Load<Sprite>(path);
            sprites[path] = sprite;
            return sprite;
        }

        public Sprite GetSpriteFromAtlas(string atlasPath, string spriteName)
        {
            Dictionary<string, Sprite> spriteSheet;

            if (sprites.ContainsKey(atlasPath))
            {
                spriteSheet = spriteSheets[atlasPath];

                if (spriteSheet.ContainsKey(spriteName))
                    return spriteSheet[spriteName];
                return null;
            }

            var rawSpriteList = Resources.LoadAll<Sprite>(atlasPath);
            spriteSheet = new Dictionary<string, Sprite>();
            foreach (var sprite in rawSpriteList)
            {
                if (spriteSheet.ContainsKey(sprite.name))
                {
                    throw new Exception(sprite.name + " уже есть в SpriteCache");
                }
                spriteSheet.Add(sprite.name, sprite);
            }
            spriteSheets[atlasPath] = spriteSheet;

            if (spriteSheet.ContainsKey(spriteName))
                return spriteSheet[spriteName];
            return null;
        }

        public void Clear()
        {
            sprites.Clear();
            spriteSheets.Clear();
        }
    }
}
