﻿using Pump.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pump.Unity
{
    public class TouchInput : PumpBehaviour
    {
        public const float longTapStartTime = 0.5f;
        public const float longTapFinishTime = 2.0f;
        public const float dragDelayTime = 0.25f;
        public const float dragThresoldFactor = 0.05f; // * Screen.height

        // temporary shortcut
        //public static TouchInput instance { get { return Singleton.Get<TouchInput>(); } }

        public List<TouchObject> touches { get { UpdateTouches(); return _touches; } }

        List<TouchObject> _touches = new();

        float lastUpdateTime;

        protected override void Awake()
        {
            base.Awake();
            callTick = true;
        }

        protected override void Tick()
        {
            UpdateTouches();
        }

        void UpdateTouches()
        {
            if (lastUpdateTime == Time.unscaledTime)
                return;
            lastUpdateTime = Time.unscaledTime;

            // remove dead touches
            _touches.RemoveAll(touch => touch.isDisposed);

            // process touches
            var rawTouches = GetRawTouches();
            foreach (var touch in rawTouches)
            {
                var touchObject = _touches.FirstOrDefault(t => t.fingerId == touch.fingerId);
                if (touchObject == null)
                {
                    //Assert.IsTrue(touch.phase == TouchPhase.Began, touch.phase.ToString());
                    touchObject = new TouchObject(touch.fingerId, touch.position, Time.unscaledTime);
                    _touches.Add(touchObject);
                }

                UpdateTouch(touchObject, touch);
            }

            // finalize dead touches
            foreach (var touch in _touches)
            {
                if (touch.nativeTouchPhase == TouchPhase.Ended || touch.nativeTouchPhase == TouchPhase.Canceled)
                    touch.Dispose();
            }
        }

        void UpdateTouch(TouchObject touchObject, Touch touch)
        {
            touchObject.nativeTouchPhase = touch.phase;
            touchObject.touchPoint = touch.position;

            if (touchObject.isTouchEnded)
                return;

            GetTouchableObjects(touchObject.touchPoint, touchObject.touchables);

            if (touchObject.activeTouchable != null && touchObject.activeTouchable.ProcessTouch(touchObject))
                return;

            touchObject.activeTouchable = null;

            foreach (var touchable in touchObject.touchables)
            {
                if (touchable.ProcessTouch(touchObject))
                {
                    touchObject.activeTouchable = touchable;
                    return;
                }
            }
        }

        void GetTouchableObjects(Vector2 touchPoint, List<ITouchable> touchables)
        {
            touchables.Clear();

            foreach (var camera in CameraManager.instance.activeCameraList)
            {
                // 2d colliders
                if (camera != CameraManager.instance.worldCamera) // skip 2d touches in 3d
                {
                    var touchPos = camera.ScreenToWorldPoint(touchPoint);
                    var hits = Physics2D.RaycastAll(touchPos, Vector2.zero);
                    foreach (var hit in hits)
                    {
                        var touchableComp = hit.collider.gameObject.GetComponentInParent<ITouchable>();

                        if (touchableComp == null || touchables.Contains(touchableComp))
                            continue;

                        touchables.Add(touchableComp);
                    }
                }

                // 3d colliders
                {
                    Ray ray = camera.ScreenPointToRay(touchPoint);
                    var hits = Physics.RaycastAll(ray);
                    Array.Sort(hits, (a, b) => Math.Sign(a.transform.position.z - b.transform.position.z));
                    foreach (var hit in hits)
                    {
                        var touchableComp = hit.collider.gameObject.GetComponentInParent<ITouchable>();

                        if (touchableComp == null || touchables.Contains(touchableComp))
                            continue;

                        touchables.Add(touchableComp);
                    }
                }
            }
        }

        List<Touch> rawTouches = new List<Touch>();

        Touch[] GetRawTouches()
        {
#if UNITY_EDITOR || UNITY_WEBGL
            rawTouches.Clear();
            // emulate touches
            for (var i = 0; i < 2; i++)
            {
                if (Input.GetMouseButtonDown(i))
                {
                    rawTouches.Add(new Touch()
                        {
                            fingerId = i,
                            position = Input.mousePosition,
                            phase = TouchPhase.Began
                        });
                }
                else if (Input.GetMouseButtonUp(i))
                {
                    rawTouches.Add(new Touch()
                        {
                            fingerId = i,
                            position = Input.mousePosition,
                            phase = TouchPhase.Ended
                        });
                }
                else if (Input.GetMouseButton(i))
                {
                    rawTouches.Add(new Touch()
                    {
                        fingerId = i,
                        position = Input.mousePosition,
                        phase = TouchPhase.Moved
                    });
                }
            }
            return rawTouches.ToArray();
#else
            return Input.touches;
#endif
        }

    }
}
