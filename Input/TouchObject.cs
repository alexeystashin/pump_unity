﻿using Pump.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public class TouchObject : IDisposable
    {
        public int fingerId;

        public TouchPhase nativeTouchPhase;

        public bool isTouchEnded;

        public bool isLongTapStarted;
        public bool isLongTapCanceled;

        public bool isDragStarted;
        //public bool isDragCanceled;


        public Vector2 touchPoint;


        public Vector2 startTouchPoint;

        public float startTouchTime;


        public List<ITouchable> touchables;

        public ITouchable activeTouchable;


        public bool isDisposed { get; private set; }

        public TouchObject(int fingerId, Vector2 touchPoint, float touchTime)
        {
            this.fingerId = fingerId;

            isTouchEnded = false;
            nativeTouchPhase = TouchPhase.Began;

            isLongTapStarted = false;
            isLongTapCanceled = false;
            isDragStarted = false;

            touchables = new();
            activeTouchable = null;

            this.touchPoint = touchPoint;

            startTouchPoint = touchPoint;
            startTouchTime = touchTime;
        }

        public override string ToString()
        {
            return $"[Touch fingerId={fingerId} touchPoint={touchPoint}]";
        }

        public void Dispose()
        {
            isDisposed = true;

            isTouchEnded = true;

            touchables.Clear();
            activeTouchable = null;
        }
    }
}
