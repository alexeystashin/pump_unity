﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public class UiTouchable : PumpBehaviour, ITouchable
    {
        bool blockTouch = true;

        bool ITouchable.ProcessTouch(TouchObject touch)
        {
            var isTouchOver = touch.touchables.Contains(this);

            if (touch.isTouchEnded || !isTouchOver)
                return isTouchOver && blockTouch;

            if (touch.nativeTouchPhase == TouchPhase.Ended)
            {
                if (isTouchOver)
                {
                    touch.isTouchEnded = true;
                    Tap();
                    return true;
                }
            }

            //Debug.Log($"Touch {gameObject.name} {touch.fingerId} {touch.nativeTouchPhase}");

            return isTouchOver && blockTouch;
        }

        void Tap()
        {
            Debug.Log($"Tap {gameObject.name}");
        }
    }
}
