﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public interface ITouchable
    {
        bool ProcessTouch(TouchObject touch);
    }
}
