﻿using UnityEngine;

namespace Pump.Unity
{
    public class CameraShake : MonoBehaviour
    {
        // Transform of the camera to shake. Grabs the gameObject's transform
        // if null.
        public Transform camTransform;

        // How long the object should shake for.
        public float shakeDuration = 1f;

        // Amplitude of the shake. A larger value shakes the camera harder.
        public float shakeAmount = 0.7f;
        public float decreaseFactor = 1.0f;

        public bool useOriginalPos = true;

        Vector3 originalPos;

        void Awake()
        {
            if (camTransform == null)
            {
                camTransform = GetComponent(typeof(Transform)) as Transform;
            }
        }

        void OnEnable()
        {
            if (camTransform != null)
                originalPos = camTransform.localPosition;
        }

        void OnDisable()
        {
            if (camTransform != null && useOriginalPos)
                camTransform.localPosition = originalPos;
        }

        void LateUpdate()
        {
            var center = useOriginalPos ? originalPos : camTransform.localPosition;

            if (shakeDuration > 0)
            {
                camTransform.localPosition = center + Random.insideUnitSphere * shakeAmount;

                shakeAmount = Mathf.Max(0, shakeAmount - Time.deltaTime * (1f - decreaseFactor));
                shakeDuration -= Time.deltaTime;
            }
            else
            {
                shakeDuration = 0f;
                camTransform.localPosition = center;
                Destroy(this);
            }
        }
    }
}
