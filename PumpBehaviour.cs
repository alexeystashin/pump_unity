﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public class PumpBehaviour : MonoBehaviour, IDisposable
    {
        public bool isInitialized { get; protected set; }

        public bool isDisposed { get; protected set; }

        public bool callTick { get; protected set; }
        public bool callLateTick { get; protected set; }
        public bool callFixedTick { get; protected set; }

        protected bool dontDestroyOnDispose;

        #region Ticks

        static List<PumpBehaviour> instances = new List<PumpBehaviour>();
        static bool needInstancesCleanup;

        public static void CallTick()
        {
            var count = instances.Count;
            for (var i = 0; i < count; i++)
            {
                var item = instances[i];
                if (item != null && !item.isDisposed && item.callTick && item.enabled && item.gameObject.activeInHierarchy)
                    item.Tick();
            }
        }

        public static void CallLateTick()
        {
            var count = instances.Count;
            for (var i = 0; i < count; i++)
            {
                var item = instances[i];
                if (item != null && !item.isDisposed && item.callLateTick && item.enabled && item.gameObject.activeInHierarchy)
                    item.LateTick();
            }
        }

        public static void CallFixedTick()
        {
            var count = instances.Count;
            for (var i = 0; i < count; i++)
            {
                var item = instances[i];
                if (item != null && !item.isDisposed && item.callFixedTick && item.enabled && item.gameObject.activeInHierarchy)
                    item.FixedTick();
            }
        }

        public static void CleanupInstances()
        {
            if (!needInstancesCleanup)
                return;

            var count = instances.Count;
            for (var i = 0; i < count; i++)
            {
                var item = instances[i];
                if (item != null && !item.isDisposed)
                    continue;

                instances.RemoveAt(i);
                i--;
                count--;
            }

            needInstancesCleanup = false;
        }

        private static void RegisterInstance(PumpBehaviour instance)
        {
            var index = instances.IndexOf(instance);
            if(index >= 0)
                throw new Exception("already registered " + (instance.gameObject != null ? instance.gameObject.name : "<null>"));

            instances.Add(instance);
        }

        private static void UnregisterInstance(PumpBehaviour instance)
        {
            var index = instances.IndexOf(instance);
            if (index < 0)
                return;

            instances[index] = null;
            needInstancesCleanup = true;
        }

        #endregion

        protected virtual void Awake()
        {
            RegisterInstance(this);
        }

        protected virtual bool Initialize()
        {
            if (isInitialized) return false;

            isInitialized = true;

            return true;
        }

        protected virtual void Start()
        {
        }

        // note: автоматически вызывается при создании объекта. в классах, отключаемых при создании, дополнительно вызывается OnDisable
        protected virtual void OnEnable()
        {
            //if (!isInitialized) return;
        }
        
        protected virtual void OnDisable()
        {
            //if (!isInitialized) return;
        }

        protected virtual void Tick()
        {
        }

        protected virtual void LateTick()
        {
        }

        protected virtual void FixedTick()
        {
        }

        protected virtual void OnDestroy()
        {
            DontDestroyOnDispose();
            Dispose();
            UnregisterInstance(this);
        }

        public void DontDestroyOnDispose()
        {
            dontDestroyOnDispose = true;
        }

        public virtual void Dispose()
        {
            if (isDisposed) return;

            isDisposed = true;

            if (dontDestroyOnDispose)
                Destroy(this);
            else
                Destroy(gameObject);
        }
    }
}