﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pump.Unity
{
    public static class Singleton
    {
        static Dictionary<Type, object> instances = new Dictionary<Type, object>();

        public static TType Get<TType>() where TType : IDisposable
        {
            object val;
            if (!instances.TryGetValue(typeof(TType), out val))
                return default(TType);
            return (TType)val;
        }

        public static bool Exists<TType>() where TType : IDisposable
        {
            return instances.ContainsKey(typeof(TType));
        }

        public static void RegisterInstance<TType>(TType objInstance) where TType : IDisposable
        {
            var type = typeof(TType);
            if (instances.ContainsKey(type))
                throw new Exception(type.Name + " already registered");
            instances.Add(type, objInstance);
        }

        public static void UnregisterInstance<TType>(TType objInstance) where TType : IDisposable
        {
            var type = typeof(TType);
            if (!instances.ContainsKey(type))
            {
                Debug.LogWarning(type.Name + " is not registered");
                return;
            }
            instances.Remove(type);
        }

        public static void ReleaseInstance<TType>(TType objInstance) where TType : IDisposable
        {
            var type = typeof(TType);
            if (!instances.ContainsKey(type))
            {
                Debug.LogWarning(type.Name + " is not registered");
                return;
            }
            UnregisterInstance<TType>(objInstance);
            objInstance.Dispose();
        }

        public static void ReleaseAll()
        {
            while (instances.Any())
            {
                var kp = instances.Last();
                instances.Remove(kp.Key);
                ((IDisposable)kp.Value).Dispose();
            }
        }
    }
}
