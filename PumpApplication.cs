﻿using Pump.Core;
using System;
using System.Collections;
using UnityEngine;

namespace Pump.Unity
{
    public class PumpApplication : PumpBehaviour
    {
        public static PumpApplication instance { get; private set; }

        protected TaskQueue initTasks;

        public bool isApplicationQuit { get; protected set; }

        protected override void Awake()
        {
            base.Awake();
            //Initialize();
        }

        protected override bool Initialize()
        {
            if (!base.Initialize()) return false;

            instance = this;
            Application.runInBackground = true;
            isApplicationQuit = false;

            //DontDestroyOnLoad(gameObject);

            initTasks = new TaskQueue();
            initTasks.events.AddListener((int)TaskEventType.TaskComplete, OnInitializationTaskComplete);

            InitBaseObjects();
            SetupInitializationTasks();

            return true;
        }

        protected virtual void InitBaseObjects()
        {
            var canvas = transform.Find("UI");
            if (canvas == null || canvas.GetComponent<Canvas>() == null)
            {
                var canvasComp = FindObjectOfType<Canvas>();
                if (canvasComp != null)
                    canvas = canvasComp.transform;
            }

            if (canvas == null)
                throw new Exception("No canvas found");

            // todo: remove below objects dependency
            var cameraManager = gameObject.AddComponent<CameraManager>();
            var appUi = canvas.gameObject.AddComponent<PumpUi>();
            var standardWindows = new StandardWindows();
        }

        protected virtual void SetupInitializationTasks()
        {
        }

        protected override void Start()
        {
            Initialize();
            initTasks.Start();
        }

        private void OnInitializationTaskComplete(IEvent e)
        {
            OnInitializationComplete();
        }

        protected virtual void OnInitializationComplete()
        {

        }

        public virtual void FatalError(string message)
        {
            DebugUtils.LogError(message);
            //Singleton.Get<UiBlocker>().Block();
            //WaitWindow.Create("FATAL ERROR", message);
        }

        public Coroutine DelayedCall(Action action, float delay)
        {
            return StartCoroutine(DelayedCallCoroutine(action, delay));
        }

        private IEnumerator DelayedCallCoroutine(Action action, float delay)
        {
            yield return new WaitForSeconds(delay);
            action();
        }

        protected virtual void Update()
        {
            PumpBehaviour.CallTick();

            if (Input.GetKeyUp(KeyCode.Escape))
                OnNativeBackButton();
        }

        protected virtual void LateUpdate()
        {
            PumpBehaviour.CallLateTick();
        }

        protected virtual void FixedUpdate()
        {
            PumpBehaviour.CallFixedTick();
            PumpBehaviour.CleanupInstances();
        }

        protected virtual void OnNativeBackButton()
        {
            var ui = PumpUi.instance;
            var currentWindow = ui.windows.currentWindow;
            var currentScreen = ui.screens.currentScreen;

            if (currentWindow != null)
                currentWindow.OnNativeBackButton();
            else if (currentScreen != null)
                currentScreen.OnNativeBackButton();
        }

        protected virtual void OnApplicationQuit()
        {
            Debug.Log("OnApplicationQuit");

            isApplicationQuit = true;

            Dispose();
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            initTasks = initTasks.SafeDispose();

            Singleton.ReleaseAll();

            base.Dispose();

            instance = null;
        }
    }
}
