﻿using Pump.Core;
using UnityEngine;

namespace Pump.Unity
{
    public class WaitTask : TaskBase
    {
        Coroutine waitCoroutine;

        float delaySec;

        public WaitTask(float delaySec)
        {
            this.delaySec = delaySec;
        }

        public override void Start()
        {
            base.Start();

            waitCoroutine = PumpApplication.instance.DelayedCall(Complete, delaySec);
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            if (waitCoroutine != null)
            {
                PumpApplication.instance.StopCoroutine(waitCoroutine);
                waitCoroutine = null;
            }

            base.Dispose();
        }
    }
}
