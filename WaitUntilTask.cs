﻿using Pump.Core;
using System.Collections;
using UnityEngine;

namespace Pump.Unity
{
    public class WaitUntilTask : TaskBase
    {
        Coroutine waitCoroutine;

        System.Func<bool> isCompleteFunc;

        public WaitUntilTask(System.Func<bool> isCompleteFunc)
        {
            this.isCompleteFunc = isCompleteFunc;
        }

        public override void Start()
        {
            base.Start();

            waitCoroutine = PumpApplication.instance.StartCoroutine(Wait());
        }

        private IEnumerator Wait()
        {
            yield return new WaitUntil(isCompleteFunc);
            Complete();
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            if (waitCoroutine != null)
            {
                PumpApplication.instance.StopCoroutine(waitCoroutine);
                waitCoroutine = null;
            }

            isCompleteFunc = null;

            base.Dispose();
        }
    }
}
