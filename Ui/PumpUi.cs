﻿using Pump.Core;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Pump.Unity
{
    public class PumpUi : PumpBehaviour
    {
        public const string uiLayerName = "UI";

        public static PumpUi instance => Singleton.Get<PumpUi>(); //todo: temporary

        public int unityUILayer { get; private set; } // const

        public bool isMouseOverGui { get; private set; }

        public Canvas canvas { get; private set; }

        public UiBlocker uiBlocker { get; private set; }
        public ScreenManager screens { get; private set; }
        public ModalWindowsManager windows { get; private set; }

        RectTransform rectTransform;

        Dictionary<UiLayerId, RectTransform> layers = new Dictionary<UiLayerId, RectTransform>();

        List<RaycastResult> raycastResults = new List<RaycastResult>();

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnDispose();

            Singleton.RegisterInstance(this);  //todo: temporary

            unityUILayer = LayerMask.NameToLayer(uiLayerName);

            callTick = true;

            canvas = GetComponent<Canvas>();
            rectTransform = GetComponent<RectTransform>();

            uiBlocker = UiBlocker.Create(GetLayer(UiLayerId.Overlay));
            uiBlocker.DontDestroyOnDispose();

            screens = gameObject.AddComponent<ScreenManager>();
            screens.DontDestroyOnDispose();

            windows = gameObject.AddComponent<ModalWindowsManager>();
            windows.Initialize(this);
            windows.DontDestroyOnDispose();
        }

        protected override void Tick()
        {
            PointerEventData ped = new PointerEventData(null);
            ped.position = Input.mousePosition; // todo: support multi-touch
            raycastResults.Clear();
            EventSystem.current.RaycastAll(ped, raycastResults);

            isMouseOverGui = raycastResults.Count > 0;
            raycastResults.Clear();
        }

        public Vector2 WorldToCanvasPoint(Vector3 worldPos, Camera camera)
        {
            return ((Vector2)camera.WorldToScreenPoint(worldPos) - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f)) / canvas.scaleFactor;
        }

        public bool IsRectTransformUnderMouse(RectTransform rectTransform, Camera camera)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(rectTransform, Input.mousePosition, camera);
        }

        public RectTransform GetLayer(UiLayerId layerId = 0)
        {
            // if layer exists - return it
            if (layers.ContainsKey(layerId))
                return layers[layerId];

            // create new layer
            var layerObject = new GameObject(layerId.ToString());
            layerObject.layer = unityUILayer;
            var layer = layerObject.AddComponent<RectTransform>();
            layer.SetParent(rectTransform, false);
            layer.StretchRectTransform();
            
            // add subcanvas
            layerObject.AddComponent<Canvas>();
            layerObject.AddComponent<GraphicRaycaster>();

            layers[layerId] = layer;

            // insert new layer between existing ones
            int nearestSiblingIndex = -1;
            foreach (var keyPair in layers)
            {
                if (layerId > keyPair.Key)
                    continue;

                var siblingIndex = keyPair.Value.GetSiblingIndex();
                if (nearestSiblingIndex < 0 || nearestSiblingIndex > siblingIndex)
                    nearestSiblingIndex = siblingIndex;
            }
            if (nearestSiblingIndex >= 0)
                layer.SetSiblingIndex(nearestSiblingIndex);

            return layer;
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            uiBlocker = uiBlocker.SafeDispose();
            screens = screens.SafeDispose();
            windows = windows.SafeDispose();

            canvas = null;
            rectTransform = null;

            base.Dispose();

            Singleton.UnregisterInstance(this); //todo: temporary
        }
    }
}