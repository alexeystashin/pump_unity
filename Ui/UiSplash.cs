﻿using UnityEngine;
using UnityEngine.UI;

namespace Pump.Unity
{
    public class UiSplash : UiElement
    {
        public static UiSplash Create(RectTransform parent = null)
        {
            var go = new GameObject("UI Splash");
            go.layer = PumpUi.instance.unityUILayer;
            var rectTransform = go.AddComponent<RectTransform>();
            var renderer = go.AddComponent<CanvasRenderer>();
            rectTransform.SetParent(parent ?? PumpUi.instance.GetLayer());
            var uiSplash = go.AddComponent<UiSplash>();
            uiSplash.Initialize();
            return uiSplash;
        }

        public Button button { get; private set; }

        protected override bool Initialize()
        {
            if (!base.Initialize()) return false;

            rectTransform.StretchRectTransform();

            gameObject.AddComponent<NonRenderingGraphic>();

            button = gameObject.AddComponent<Button>();
            var nav = button.navigation;
            nav.mode = Navigation.Mode.None;
            button.navigation = nav;
            button.transition = Selectable.Transition.None;

            return true;
        }

        public override void Dispose()
        {
            if (button != null)
            {
                button.onClick = null;
                button = null;
            }

            base.Dispose();
        }
    }
}