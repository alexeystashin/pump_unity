﻿using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public class UiBlocker : UiElement
    {
        public static UiBlocker Create(RectTransform parentTransform)
        {
            var go = GameObjectUtils.CreateObject("UI Blocker", parentTransform);
            var rt = go.AddComponent<RectTransform>();
            rt.StretchRectTransform();
            go.AddComponent<CanvasRenderer>();
            go.AddComponent<NonRenderingGraphic>();
            go.SetActive(false);

            var uiBlocker = go.AddComponent<UiBlocker>();
            uiBlocker.Initialize();
            return uiBlocker;
        }

        public bool isBlocked => blockCounter > 0;

        int blockCounter;

        Dictionary<object,int> blockObjects = new Dictionary<object, int>();

        public void Block(object blockObject = null)
        {
            if (blockObject != null)
            {
                if (blockObjects.ContainsKey(blockObject))
                    blockObjects[blockObject] += 1;
                else
                    blockObjects.Add(blockObject, 1);
            }

            blockCounter++;
            //Debug.Log("BLOCK " + _blocks);
            gameObject.SetActive(blockCounter > 0);
        }

        public void Unblock(object blockObject = null)
        {
            if (blockObject != null && blockObjects.ContainsKey(blockObject))
            {
                if (blockObjects.ContainsKey(blockObject))
                    blockObjects[blockObject] -= 1;
                else
                    blockObjects.Add(blockObject, -1);
            }

            blockCounter--;
            //Debug.Log("UNBLOCK " + _blocks);
            gameObject.SetActive(isBlocked);
        }

        public void UnblockAll(object blockObject = null)
        {
            if (blockObject == null || !blockObjects.ContainsKey(blockObject))
                return;

            blockCounter -= blockObjects[blockObject];

            blockObjects.Remove(blockObject);

            //Debug.Log("UNBLOCK " + _blocks);
            gameObject.SetActive(isBlocked);
        }

        public override void Dispose()
        {
            if (blockCounter > 0)
                Debug.LogWarning($"UI blocked by {blockCounter} objects");
            blockCounter = 0;
            blockObjects.Clear();
            base.Dispose();
        }
    }
}
