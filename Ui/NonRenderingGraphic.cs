﻿using UnityEngine;
using UnityEngine.UI;

/// Credit Slipp Douglas Thompson 
/// Sourced from - https://gist.github.com/capnslipp/349c18283f2fea316369
// https://unity-ui-extensions.github.io/Controls/NonDrawingGraphic.html

namespace Pump.Unity
{
    /// A concrete subclass of the Unity UI `Graphic` class that just skips drawing.
    /// Useful for providing a raycast target without actually drawing anything.
    [RequireComponent(typeof(CanvasRenderer))]
    public class NonRenderingGraphic : Graphic
    {
        public override void SetMaterialDirty()
        {
            return;
        }

        public override void SetVerticesDirty()
        {
            return;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
            return;
        }
    }
}