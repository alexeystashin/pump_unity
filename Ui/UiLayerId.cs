﻿namespace Pump.Unity
{
    public enum UiLayerId : int
    {
        PoolContainer = -10,

        SceneUI = -5,

        Default = 0,

        Screens = 11,

        Windows = 100,

        Drag = 150,

        Tutorial = 160,

        Overlay = 200
    }
}