﻿using System;
using UnityEngine;

namespace Pump.Unity
{
    public class ModalWindow : UiElement
    {
        public Action onOutsideClick { get; protected set; }

        public static T Create<T>(GameObject prefab, bool initialize = true, bool setActive = true) where T : ModalWindow
        {
            return UiElement.Create<T>(prefab, PumpUi.instance.GetLayer(UiLayerId.Windows), initialize, setActive);
        }

        public virtual void OnNativeBackButton()
        {
        }

        protected override void OnEnable()
        {
            //Debug.Log(GetType().Name + ".OnEnable " + isInitialized);

            if (!isInitialized) return;

            base.OnEnable();

            var windowsManager = PumpUi.instance.windows;
            if(windowsManager != null)
                windowsManager.AddItem(this);
        }

        protected override void OnDisable()
        {
            //Debug.Log(GetType().Name + ".OnDisable " + isInitialized);

            if (!isInitialized) return;

            var windowsManager = PumpUi.instance.windows;
            if (windowsManager != null)
                windowsManager.RemoveItem(this);

            base.OnDisable();
        }

		public override void Dispose()
        {
            if (isDisposed) return;

            onOutsideClick = null;

            base.Dispose();
        }
    }
}