﻿using Pump.Core;
using System;
using System.Collections.Generic;

namespace Pump.Unity
{
    public class ModalWindowsManager : PumpBehaviour
    {
        private List<ModalWindow> _items = new List<ModalWindow>();

        private UiSplash _backgroundSplash;

        public ModalWindow currentWindow { get { return _items.Count>0 ? _items[_items.Count-1] : null; } }

        public event Action onModalWindowChanged;

        public bool Initialize(PumpUi pumpUi)
        {
            if (!Initialize())
                return false;

            _backgroundSplash = UiSplash.Create(pumpUi.GetLayer(UiLayerId.Windows));
            _backgroundSplash.gameObject.SetActive(false);
            _backgroundSplash.button.onClick.AddListener(OnBackgroundClick);

            return true;
        }

        private void OnBackgroundClick()
        {
            if (_items.Count == 0)
                return;

            var currentWindow = _items[_items.Count - 1];
            if (currentWindow.onOutsideClick != null)
                currentWindow.onOutsideClick();
        }

        public void AddItem(ModalWindow item)
        {
            if (_items.Contains(item))
                throw new InvalidOperationException();

            _items.Add(item);

            _backgroundSplash.gameObject.SetActive(true);
            _backgroundSplash.rectTransform.SetAsLastSibling();
            item.rectTransform.SetAsLastSibling();

            onModalWindowChanged?.Invoke();
        }

        public void RemoveItem(ModalWindow item)
        {
            if (!_items.Contains(item))
                throw new InvalidOperationException();

            _items.Remove(item);

            if (_items.Count > 0)
            {
                _backgroundSplash.rectTransform.SetAsLastSibling();
                _items[_items.Count - 1].rectTransform.SetAsLastSibling();
            }
            else
            {
                _backgroundSplash.gameObject.SetActive(false);
            }
            onModalWindowChanged?.Invoke();
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            var leakedWIndows = new List<ModalWindow>(_items);
            _items.Clear();
            foreach (var modalWindow in leakedWIndows)
                modalWindow.Dispose();

            _backgroundSplash.SafeDispose();

            base.Dispose();
        }
    }
}