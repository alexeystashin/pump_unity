﻿using System;
using Pump.Core;

namespace Pump.Unity
{
    public class StandardWindows : IDisposable
    {
        public static StandardWindows instance => Singleton.Get<StandardWindows>();  //todo: temporary

        public delegate ModalWindow CreateMessageWindowFunc(string caption, string description, Action onOk = null);
        public delegate ModalWindow CreateYesNoWindowFunc(string caption, string description, Action onYes = null, Action onNo = null, string yesText = null, string noText = null);
        public delegate ModalWindow CreateWaitWindowFunc(string caption, string description, Action onCancel = null);

        CreateMessageWindowFunc createMessageWindowFunc;
        CreateYesNoWindowFunc createYesNoWindowFunc;
        CreateWaitWindowFunc createWaitWindowFunc;

        bool isDisposed;

        public StandardWindows()
        {
            Singleton.RegisterInstance(this); //todo: temporary
        }

        public void SetCreateMessageWindowFunc(CreateMessageWindowFunc func)
        {
            createMessageWindowFunc = func;
        }

        public void SetCreateYesNoWindowFunc(CreateYesNoWindowFunc func)
        {
            createYesNoWindowFunc = func;
        }

        public void SetCreateWaitWindowFunc(CreateWaitWindowFunc func)
        {
            createWaitWindowFunc = func;
        }

        public ModalWindow ShowMessageWindow(string caption, string description, Action onOk = null)
        {
            if (createMessageWindowFunc == null)
            {
                DebugUtils.LogError("createMessageWindowFunc is not set");
                return null;
            }
            return createMessageWindowFunc(caption, description, onOk);
        }

        public ModalWindow ShowYesNoWindow(string caption, string description, Action onYes = null, Action onNo = null, string yesText = null, string noText = null)
        {
            if (createYesNoWindowFunc == null)
            {
                DebugUtils.LogError("createYesNoWindowFunc is not set");
                return null;
            }
            return createYesNoWindowFunc(caption, description, onYes, onNo, yesText, noText);
        }

        public ModalWindow ShowWaitWindow(string caption, string description, Action onCancel = null)
        {
            if (createWaitWindowFunc == null)
            {
                DebugUtils.LogError("createMessageWindowFunc is not set");
                return null;
            }
            return createWaitWindowFunc(caption, description, onCancel);
        }

        public void Dispose()
        {
            if (isDisposed) return;

            isDisposed = true;

            createMessageWindowFunc = null;
            createYesNoWindowFunc = null;
            createWaitWindowFunc = null;

            Singleton.UnregisterInstance(this); //todo: temporary
        }
    }
}