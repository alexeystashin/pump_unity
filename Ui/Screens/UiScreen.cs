﻿using UnityEngine;

namespace Pump.Unity
{
    public class UiScreen : UiElement
    {
        public static T Create<T>(GameObject prefab, bool initialize = true, bool setActive = false) where T : UiScreen
        {
            return UiElement.Create<T>(prefab, PumpUi.instance.GetLayer(UiLayerId.Screens), initialize, setActive);
        }

        public virtual void OnNativeBackButton()
        {
        }
    }
}