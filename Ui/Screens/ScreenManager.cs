﻿using Pump.Core;
using UnityEngine;

namespace Pump.Unity
{
    public class ScreenManager : PumpBehaviour, IHaveEvents
    {
        public EventMessenger events { get; private set; }

        public UiScreen currentScreen { get; private set; }

        private Vector2 screenSize;

        protected override void Awake()
        {
            base.Awake();
            events = new EventMessenger();
            callTick = true;
        }

        protected override void Tick()
        {
            //base.Tick();
            if (screenSize.x != Screen.width || screenSize.y != Screen.height)
            {
                screenSize = new Vector2(Screen.width, Screen.height);
                events.Raise(new ScreenEvent(ScreenEventType.ScreenSizeChanged, currentScreen, currentScreen));
            }
        }

        public void GotoScreen(UiScreen screen)
        {
            var prevScreen = currentScreen;

            events.Raise(new ScreenEvent(ScreenEventType.ScreenChanging, screen, prevScreen));

            if (currentScreen!=null && currentScreen != screen)
                currentScreen.gameObject.SetActive(false);

            currentScreen = screen;

            if (currentScreen != null)
                currentScreen.gameObject.SetActive(true);

            events.Raise(new ScreenEvent(ScreenEventType.ScreenChanged, currentScreen, prevScreen));
        }

        public override void Dispose()
        {
            base.Dispose();
            currentScreen = null;
            events = events.SafeDispose();
        }
    }
}