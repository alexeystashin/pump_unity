﻿using Pump.Core;

namespace Pump.Unity
{
    public enum ScreenEventType : int
    {
        _First = 30000,

        ScreenChanging,
        ScreenChanged,
		ScreenSizeChanged
	}

    public class ScreenEvent : IEvent
    {
        public int eventType { get; private set; }

        public UiScreen screen { get { return _screen.Obj; } }
        public UiScreen prevScreen { get { return _prevScreen.Obj; } }

        private WeakRef<UiScreen> _screen = new WeakRef<UiScreen>();
        private WeakRef<UiScreen> _prevScreen = new WeakRef<UiScreen>();

        public ScreenEvent(ScreenEventType eventType, UiScreen screen, UiScreen prevScreen)
        {
            this.eventType = (int)eventType;
            _screen.Set(screen);
            _prevScreen.Set(prevScreen);
        }
    }
}