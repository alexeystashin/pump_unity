﻿using UnityEngine;

namespace Pump.Unity
{
    public class UiElement : PumpBehaviour
    {
        public RectTransform rectTransform { get; private set; }

        public static T Create<T>(GameObject prefab, RectTransform parent = null, bool initialize = true, bool setActive = true) where T : UiElement
        {
            var go = Instantiate(prefab);
            go.transform.SetParent(parent ?? PumpUi.instance.GetLayer(),false);

            go.SetActive(false);

            var uiElement = go.GetOrAddComponent<T>();

            // warning: Awake is not called yet!!!
            if (initialize)
                uiElement.Initialize();

            if (setActive)
                go.SetActive(true);

            return uiElement;
        }

        public static T Create<T>(RectTransform parent = null, bool initialize = true, bool setActive = true) where T : UiElement
        {
            var go = new GameObject(typeof(T).Name);
            go.AddComponent<RectTransform>();
            go.transform.SetParent(parent ?? PumpUi.instance.GetLayer(),false);

            go.SetActive(false);

            var uiElement = go.AddComponent<T>();

            // warning: Awake еще не вызван!!!
            if (initialize)
                uiElement.Initialize();

            if (setActive)
                go.SetActive(true);

            return uiElement;
        }

        protected override bool Initialize()
        {
            if (!base.Initialize()) return false;

            rectTransform = GetComponent<RectTransform>();

            return true;
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            rectTransform = null;

            base.Dispose();
        }
    }
}