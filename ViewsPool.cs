﻿using Pump.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public interface IViewsPool : IDisposable
    {
        GameObject Take(string prefabId);
        void Release(string prefabId, GameObject go);
    }

    public class ViewsPool : IViewsPool
    {
        Dictionary<string, ObjectPool<GameObject>> viewsPools = new Dictionary<string, ObjectPool<GameObject>>();

        Transform poolContainer;

        IUnityPrefabProvider prefabProvider;

        public event Action<GameObject> OnCreate;
        public event Action<GameObject> OnRelease;
        public event Action<GameObject> OnDestroy;

        public ViewsPool(IUnityPrefabProvider prefabProvider)
            : this(prefabProvider, null)
        {
        }

        public ViewsPool(IUnityPrefabProvider prefabProvider, Transform parent)
        {
            this.prefabProvider = prefabProvider;

            var go = GameObjectUtils.CreateObject("ObjectPool", parent);
            GameObject.DontDestroyOnLoad(go);
            go.SetActive(false);
            poolContainer = go.transform;
        }

        public GameObject Take(string prefabId)
        {
            if (!viewsPools.ContainsKey(prefabId))
                viewsPools[prefabId] = CreatePool(prefabId);

            var viewGo = viewsPools[prefabId].Take();
            viewGo.SetActive(true);
            return viewGo;
        }

        public void Release(string prefabId, GameObject go)
        {
            if (!viewsPools.TryGetValue(prefabId, out var pool))
                return;
            pool.Release(go);
        }

        ObjectPool<GameObject> CreatePool(string prefab)
        {
            return new ObjectPool<GameObject>(() => Pool_CreateViewGO(prefab), Pool_ReleaseViewGO, Pool_DestroyViewGO, 0);
        }

        GameObject Pool_CreateViewGO(string prefab)
        {
            if (PumpApplication.instance != null && PumpApplication.instance.isApplicationQuit)
                    throw new Exception($"Create view while application quit. ({prefab})");

            var viewGo = GameObjectUtils.CreateObject(prefabProvider.GetPrefab(prefab), null, poolContainer);

            if (OnCreate != null)
                OnCreate.Invoke(viewGo);

            return viewGo;
        }

        void Pool_ReleaseViewGO(GameObject viewGo)
        {
            if (PumpApplication.instance != null && PumpApplication.instance.isApplicationQuit)
            {
                Debug.LogWarning($"Release view while application quit. Skipped. ({viewGo.name})");
                return;
            }

            if (OnRelease != null)
                OnRelease.Invoke(viewGo);

            viewGo.transform.SetParent(poolContainer, false);
            viewGo.SetActive(false);
        }

        void Pool_DestroyViewGO(GameObject viewGo)
        {
            if (OnDestroy != null)
                OnDestroy.Invoke(viewGo);

            GameObject.Destroy(viewGo);
        }

        public void Dispose()
        {
            poolContainer = poolContainer.SafeDestroy();
            foreach (var pool in viewsPools.Values)
                pool.Dispose();
            viewsPools.Clear();
            prefabProvider = null;
        }
    }
}
