﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

namespace Pump.Unity
{
    public static class GameObjectUtils
    {
        // return full GameObject path in hierarchy 
        public static string GetGameObjectPath(GameObject obj)
        {
            if (obj == null) return "null";

            var path = new StringBuilder();
            path.Append("/" + obj.name);
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path.Insert(0, "/" + obj.name);
            }
            return path.ToString();
        }

        public static string GetGameObjectPath(Component objComponent)
        {
            if (objComponent == null) return "null";
            return GetGameObjectPath(objComponent.gameObject);
        }

        public static T GetOrAddComponent<T>(this Transform transform) where T : Component
        {
            return transform.gameObject.GetOrAddComponent<T>();
        }

        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component == null)
                component = gameObject.AddComponent<T>();
            return component;
        }

        public static TComponent FindComponentInChildren<TComponent>(this Transform transform, int deep = 0) where TComponent : Component
        {
            var component = transform.GetComponent<TComponent>();
            if (component)
                return component;

            if (deep > 0)
            {
                deep--;

                for (var i = 0; i < transform.childCount; i++)
                {
                    component = FindComponentInChildren<TComponent>(transform.GetChild(i), deep);
                    if (component)
                        return component;
                }
            }

            return null;
        }

        public static void ResetTransform(this Transform transform)
        {
            if (transform is RectTransform rectTransform)
            {
                rectTransform.anchoredPosition = Vector2.zero;
                rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
                rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
            }
            else
                transform.localPosition = Vector3.zero;

            transform.localRotation = Quaternion.Euler(Vector3.zero);
            transform.localScale = Vector3.one;
        }

        // растягивает RectTransform на максимум
        public static void StretchRectTransform(this RectTransform rectTransform)
        {
            rectTransform.localPosition = Vector3.zero;
            rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
            rectTransform.localScale = Vector3.one;

            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
        }

        public static void SetLayerRecursive(this GameObject go, int layer)
        {
            go.layer = layer;
            var count = go.transform.childCount;
            for (var i = 0; i < count; i++)
            {
                var t = go.transform.GetChild(i);
                t.gameObject.layer = layer;
                if (t.childCount > 0)
                    SetLayerRecursive(t.gameObject, layer);
            }
        }

        public static void SetSortingGroup(this GameObject go, string layerName, int order)
        {
            SetSortingGroup(go, SortingLayer.NameToID(layerName), order);
        }

        public static void SetSortingGroup(this GameObject go, int layerId, int order)
        {
            var sortingGroup = go.GetOrAddComponent<SortingGroup>();
            sortingGroup.sortingLayerID = layerId;
            sortingGroup.sortingOrder = order;
        }

        public static T CreateObject<T>(string name, Transform parent=null) where T : Component
        {
            var go = new GameObject(name);
            if(parent != null)
                go.transform.SetParent(parent,false);
            var component = go.AddComponent<T>();
            return component;
        }

        public static T CreateObject<T>(GameObject prefab, string name, Transform parent=null) where T : Component
        {
            var go = GameObject.Instantiate(prefab, parent, false);
            if (!string.IsNullOrEmpty(name))
                go.name = name;
            var component = go.GetOrAddComponent<T>();
            return component;
        }

        public static GameObject CreateObject(string name, Transform parent=null)
        {
            var go = new GameObject(name);
            if(parent != null)
                go.transform.SetParent(parent,false);
            return go;
        }

        public static GameObject CreateObject(GameObject prefab, string name, Transform parent=null)
        {
            var go = GameObject.Instantiate(prefab, parent, false);
            if (!string.IsNullOrEmpty(name))
                go.name = name;
            return go;
        }

        public static GameObject SafeDestroy(this GameObject gameObject)
        {
            if (gameObject)
                GameObject.Destroy(gameObject);
            return null;
        }

        public static TComponent SafeDestroy<TComponent>(this TComponent component) where TComponent : Component
        {
            if (component)
                GameObject.Destroy(component.gameObject);
            return null;
        }

        public static void DestroyAndClear(this IList<GameObject> items)
        {
            foreach (var item in items)
                GameObject.Destroy(item);
            items.Clear();
        }

        public static void DestroyAndClear<T>(this IList<T> items) where T : MonoBehaviour
        {
            foreach (var item in items)
                GameObject.Destroy(item.gameObject);
            items.Clear();
        }

        [Obsolete]
        public static Camera GetCamera(this GameObject go)
        {
            var camera = go.GetComponentInParent<Camera>();
            if (camera != null)
                return camera;
            //return CameraManager.instance.worldCamera;
            return Camera.main;
        }

        public static string BeautifyName(string id)
        {
            if (string.IsNullOrEmpty(id))
                return string.Empty;

            var str = new StringBuilder();
            var prevIsChar = false;
            var prevIsNum = false;
            foreach (var c in id)
            {
                if (c == '_')
                {
                    str.Append(' ');
                    prevIsChar = false;
                    prevIsNum = false;
                    continue;
                }
                if (char.IsUpper(c) && prevIsChar)
                    str.Append(' ');
                else if (char.IsNumber(c) && prevIsChar && !prevIsNum)
                    str.Append(' ');
                str.Append(c);
                prevIsChar = !char.IsWhiteSpace(c);
                prevIsNum = char.IsNumber(c);
            }
            return str.ToString();
        }
    }
}
