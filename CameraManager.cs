﻿using System.Collections.Generic;
using UnityEngine;

namespace Pump.Unity
{
    public class CameraManager : PumpBehaviour
    {
        public static CameraManager instance => Singleton.Get<CameraManager>();  //todo: temporary

        public List<Camera> activeCameraList;

        public Camera worldCamera { get; private set; }
        public Camera uiCamera { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnDispose();

            Singleton.RegisterInstance(this);  //todo: temporary

            worldCamera = Camera.main;
            uiCamera = worldCamera;
            activeCameraList = new List<Camera> { worldCamera };
        }

        public void SetWorldCamera(Camera camera)
        {
            worldCamera = camera;
            RefreshActiveCameraList();
        }

        public void SetUiCamera(Camera camera)
        {
            uiCamera = camera;
            RefreshActiveCameraList();
        }

        void RefreshActiveCameraList()
        {
            if(worldCamera == null && uiCamera == null)
                activeCameraList = new List<Camera> { };
            else if (worldCamera == uiCamera)
                activeCameraList = new List<Camera> { worldCamera };
            else if (worldCamera == null)
                activeCameraList = new List<Camera> { uiCamera };
            else if (uiCamera == null)
                activeCameraList = new List<Camera> { worldCamera };
            else
                activeCameraList = new List<Camera> { uiCamera, worldCamera };
        }

        public override void Dispose()
        {
            if (isDisposed) return;

            base.Dispose();

            Singleton.UnregisterInstance(this); //todo: temporary
        }
    }
}
